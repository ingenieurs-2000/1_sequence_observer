# Industrie 4.0 : Séquence Observer

*Ingénieur 2000 / Référent Julien NOYER*

![](https://i.imgur.com/ICSQuJT.png)


*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---


<br>

# Définition du répertoire

Le contenu de ce répertoire à pour objectif de servir de base pour la réalisation d'une page Web intégrant du design CSS et des interactions Javascript.

- **Supports de cours** https://hackmd.io/@school-inge-2000/SkFxZIvVd
- **Lien vers la version finale** https://map.dwsapp.io

<br>

## Utilisation du répertoire

Le fichier `socket.html` à besoin de la technologie NodeJS pour fonctionner. C'est pourquoi vous devez au préalable vous assurer que les modules `node` et `npm` sont installés avant de taper les commandes suivante dans votre terminal à la racine de votre dossier local : 

```
npm i
npm start 
```

> La totalité des pages sont alors disponible à l'adresse http://localhost:3017

<br>

Pour les autres fichiers vous n'avez pas besoin de NodeJS mais il est toujours intéressant de mettre en place une logique serveur dans le cadre du développement d'une page Web. Nous vous conseillons donc d'installer **LiveServer** sur votre machine avec la commande suivante : 

```
sudo npm i -g live-server
```
<br>

Il suffi ensuite de vous placer dans le dossier dans lequel vous souhaitez lancer votre environnement serveur et taper la commande suivante : 

```
live-server
```

> L'un des interêts de **LiveServer** est qu'à chaque modification du code, la page se recharge dans le navigateur.

---

<br><br><br>

# Notation de la séquence

![](https://i.imgur.com/FC9fnu5.png)

<br>

En plus du formulaire général sur les notions en Data aborder pendant la séquence "**Observer**", vous aurez à réaliser des **exercices notés sur 20** dans lesquels vous devrez mettre en pratique les techniques abordées lors des sessions de cours avec Julien Noyer.

<br>

## Enoncé des exercices

Vous devez réaliser les **4 exercices** défini dans le fichier `exercice.html` dans le répertoire GitLab. Chacun de ces exercices vous rapporte **5 points** afin de vous donner une notre sur 20. Vous pouvez cliquer sur le lien suivant pour accéder directement au fichier `exercice.html` : 

- https://gitlab.com/ingenieurs-2000/1_sequence_observer/-/blob/master/exercice.html

<br>

Un exercice alternatif vous est également proposé dans le fichier `exercice.html` qui, si vous le réussissez, vous permet d'atteindre **20 points** sans avoir à faire les autres exercices. Dans cette exercice, qui se nomme **Kobayashi Maru** en hommage à la conquête spatiale, vous devez mettre en place un système de communication en temps réel dans une page Web en utilisant la bibliothèque Socket.io.

<br>

## Restitution

Pour restituer votre travail vous devez réaliser un fichier **ZIP** de votre dossier de travail afin que votre correcteur puisse tester le code dans son environnement local.

- **Date de restitution** 12 avril 2021

<br>

Pour créer une archive ZIP sur Mac, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Compresser...". Pour créer une archive ZIP sur Windows, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Envoyer vers > Ficher compressé".

- Créer un fichier **ZIP sur Mac** : https://apple.co/30W1cul
- Créer un fichier **ZIP sur Windows** : https://bit.ly/3sgc3vf

<br>

Une fois votre archive créée, il est essentiel que vous respectiez la nomenclature suivante pour nommé votre archive :

- **NOM_PRENOM_JS_OBSERVER**

<br>

Vous devez ensuite effectuer votre rendu via le LMS Ingénieur2000 en y déposant votre fichier **ZIP** dans le dossier correspondant à la séquence.

--- 

<br><br><br>

# Ressources

![](https://i.imgur.com/eAySYs0.png)

Liste de liens utiles pour la mise en place de la séquence : 

- **Google Sheet** https://docs.google.com/spreadsheets
- **GlideApp** https://www.glideapps.com
- **Hypertext Markup Language** https://hackmd.io/@dws-teach/SyO1wn858
- **Cascading Style Sheets** https://hackmd.io/@dws-teach/BJO72aLqL
- **Markdown** https://fr.wikipedia.org/wiki/Markdown
- **CommonMark** https://commonmark.org
- **Hackmd** https://hackmd.io/
- **Golang** https://golang.org
- **Homebrew** https://brew.sh/index_fr
- **SocketIO** https://socket.io

<br>

---

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) - All rights reserved for educational purposes only*

---