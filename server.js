/* 
Imports
*/
    const express = require('express');
    const http = require('http');
    const socket = require('socket.io');
//

/* 
Serveur configuration
*/
    // Serveur variables
    const app = express();
    const server = http.Server(app);
    const io = socket(http);
    const port = 3017;

    // Set static folder
    app.set( 'views', __dirname );
    app.use( express.static(__dirname) );
//

/* 
Socket configuration
*/
    // Bind connection between the client and the server
    io.on('connection', (socket) => {
        // Get message from topic TOPIC_new_message subscription
        socket.on('TOPIC_new_message', input => {
            // Publish data on topic TOPIC_new_message
            io.emit('TOPIC_new_message', { msg: input, userId: socket.id });
        });
    });
//

/* 
Launch server
*/
    server.listen(port, () => {
        console.log(`Socket.IO server running at http://localhost:${port}/`);
    });
//